class PostsController < ApplicationController
  protect_from_forgery :except => [:index, :create, :new]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json

  def index
    if admin_signed_in?
      authenticate_admin!
      @posts = Post.all
        respond_to do |format|
          format.json do
            render :json => @posts, :callback => params[:callback]
          end
        format.html
      end
    else
      authenticate_user!
      @posts = current_user.posts
    end
  end  
  # GET /posts/1
  # GET /posts/1.json

  def show
    @posts = Post.all
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    if admin_signed_in?
      if cannot? :edit, @post
        flash[:alert]= "No tienes permisos para editar el post"
        redirect_to :action => 'index', :format=>'html'
      end
    end
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user_id = 1

    respond_to do |format|
      if @post.save
        flash[:success]="Guardado correctamente"
        format.html{redirect_to @post}
        #format.html { redirect_to @post, notice: 'Post was successfully created.' }
        #format.json { render :show, status: :created, location: @post }
        format.json do
          render :json=>@post, :callback=> params[:callbak]
        end
      else
        format.html { render :new }
        flash[:alert]="No se pudo guardar"        
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        flash[:success]="Se actualizo correctamente"
        format.html{redirect_to @post}
        format.json { render :show, status: :ok, location: @post }
      else
        format.html{render :edit}
        flash[:alert]="Imposible actualizar"        
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      flash[:alert]="Eliminado correctamente"
      format.html{redirect_to posts_url}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.permit(:title, :body)
    end
end
